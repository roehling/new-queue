#!/usr/bin/python3
#
# Copyright 2021 Timo Röhling <roehling@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
import argparse
from collections import defaultdict
from datetime import datetime, timedelta
import logging
import lzma
from pathlib import Path
import re
import time
import sqlite3
import sys


OPEN_FUNC = {".xz": lzma.open, "": open}


def loglines(log_dirs, start_at=None, end_at=None):
    for log_dir in log_dirs:
        if log_dir.is_dir():
            logging.info(f"Found log files in {log_dir}")
            logfiles = [
                f for f in log_dir.iterdir() if re.match(r"\d{4}-\d{2}(?:$|\.xz)", f.name)
            ]
            break
    else:
        raise FileNotFoundError("cannot find log files")
    logfiles.sort()
    # Logs before September 2012 have incompatible differences which I am too lazy to support
    logfiles = [f for f in logfiles if f.name[:7] >= "2012-09"]
    if start_at is not None:
        start_date = start_at.strftime("%Y%m%d%H%M%S")
        year_mon = start_at.strftime("%Y-%m")
        logfiles = [f for f in logfiles if f.name[:7] >= year_mon]
    else:
        start_date = "00000000000000"
    if end_at is not None:
        end_date = end_at.strftime("%Y%m%d%H%M%S")
        year_mon = end_at.strftime("%Y-%m")
        logfiles = [f for f in logfiles if f.name[:7] <= year_mon]
    else:
        end_date = "99999999999999"
    for fname in logfiles:
        logging.info(f"Reading {fname.name}")
        with OPEN_FUNC[fname.suffix](str(fname), "rt", errors="replace") as f:
            for line in f:
                timestamp = line[:14]
                if timestamp >= start_date and timestamp <= end_date:
                    yield line


def queue_events(db, start_at=None, end_at=None):
    queued_changes = {}
    source_packages = set()
    uploaded = 0
    actions = defaultdict(int)
    if start_at is None:
        start_at = datetime(1, 1, 1)
    if end_at is None:
        end_at = datetime(9999, 1, 1)

    with db as sql:
        for time, action, changes in sql.execute(
            "SELECT time, action, changes FROM queue_events_1 WHERE time >= ? AND time <= ? ORDER BY time",
            (start_at, end_at),
        ):
            if action == "ACCEPT-TO-NEW":
                if changes not in queued_changes:
                    package_name = changes.split("_", 1)[0]
                    binNEW = package_name in source_packages
                    queued_changes[changes] = time
                    uploaded += 1
                    yield "NEW", binNEW, time, None, changes
                continue
            mo = re.search(r"(ACCEPT|REJECT)", action)
            if mo:
                if changes in queued_changes:
                    package_name = changes.split("_", 1)[0]
                    binNEW = package_name in source_packages
                    if mo.group(1) == "ACCEPT":
                        source_packages.add(package_name)
                    entry_time = queued_changes.pop(changes)
                    actions[mo.group(1)] += 1
                    yield mo.group(1), binNEW, entry_time, time, changes
                continue
    logging.debug(f"Total uploads: {uploaded:>8}")
    for key, value in actions.items():
        logging.debug(f"Total {key}s: {value:>8}")
    logging.debug(f"Still in NEW:  {len(queued_changes):>8}")
    oldest_change = min(queued_changes.values())
    logging.info(f"Oldest unprocessed upload: {oldest_change}")
    #with open("source_packages.txt", "w") as f:
    #    f.write("\n".join(sorted(source_packages)))


def parse_log_files_to_database(db, log_dirs, start_at=None, end_at=None):
    with db as sql:
        for line in loglines(log_dirs, start_at=start_at, end_at=end_at):
            fields = line.split("|")
            if len(fields) < 5:
                continue
            if fields[1][:8] != "process-":
                continue
            if re.search(r"NEW|ACCEPT|REJECT", fields[3]):
                fields[-1] = fields[-1].rstrip()
                changes = next((f for f in fields if f[-8:] == ".changes"), None)
                if changes:
                    sql.execute(
                        "INSERT INTO queue_events_1 VALUES (?, ?, ?, ?, ?)",
                        (
                            datetime.strptime(fields[0], "%Y%m%d%H%M%S"),
                            fields[1],
                            fields[2],
                            fields[3],
                            changes,
                        ),
                    )
                else:
                    logging.warning(
                        f"No valid .changes file associated with {fields[3]!r} at {fields[0]}"
                    )


def update_database(db, log_dirs):
    start_at = None
    with db as sql:
        sql.execute(
            "CREATE TABLE IF NOT EXISTS queue_events_1 "
            "(time TIMESTAMP, tool STRING, user STRING, action STRING, changes STRING)"
        )
        sql.execute("CREATE INDEX IF NOT EXISTS time_index ON queue_events_1 (time)")
        result = next(
            sql.execute("SELECT time FROM queue_events_1 ORDER BY time DESC LIMIT 1"),
            None,
        )
        if result is not None:
            start_at = result[0]
            start_at = datetime(start_at.year, start_at.month, 1)
            sql.execute("DELETE FROM queue_events_1 WHERE time >= ?", (start_at,))
    parse_log_files_to_database(db, log_dirs, start_at)


def eval_by_outcome(db, output):
    accept_times = []
    bin_accept_times = []
    reject_times = []
    bin_reject_times = []
    for action, binNEW, entry_time, exit_time, changes_file in queue_events(db):
        if action == "ACCEPT":
            if binNEW:
                bin_accept_times.append(exit_time - entry_time)
            else:
                accept_times.append(exit_time - entry_time)
        if action == "REJECT":
            if binNEW:
                bin_reject_times.append(exit_time - entry_time)
            else:
                reject_times.append(exit_time - entry_time)
    logging.info(
        f"Processed {len(accept_times)} ACCEPTs, {len(bin_accept_times)} binNEW ACCEPTs, "
        f"{len(reject_times)} REJECTs, and {len(bin_reject_times)} binNEW REJECTs"
    )

    accept_times.sort()
    bin_accept_times.sort()
    reject_times.sort()
    bin_reject_times.sort()

    accept_50 = accept_times[int(0.50 * len(accept_times))]
    accept_90 = accept_times[int(0.90 * len(accept_times))]
    accept_98 = accept_times[int(0.98 * len(accept_times))]

    bin_accept_50 = bin_accept_times[int(0.50 * len(bin_accept_times))]
    bin_accept_90 = bin_accept_times[int(0.90 * len(bin_accept_times))]
    bin_accept_98 = bin_accept_times[int(0.98 * len(bin_accept_times))]

    reject_50 = reject_times[int(0.50 * len(reject_times))]
    reject_90 = reject_times[int(0.90 * len(reject_times))]
    reject_98 = reject_times[int(0.98 * len(reject_times))]

    bin_reject_50 = bin_reject_times[int(0.50 * len(bin_reject_times))]
    bin_reject_90 = bin_reject_times[int(0.90 * len(bin_reject_times))]
    bin_reject_98 = bin_reject_times[int(0.98 * len(bin_reject_times))]

    output.write(
        "ACCEPTs\n"
        f"    50% - {accept_50}\n"
        f"    90% - {accept_90}\n"
        f"    98% - {accept_98}\n"
        "\n"
        "binNEW ACCEPTs\n"
        f"    50% - {bin_accept_50}\n"
        f"    90% - {bin_accept_90}\n"
        f"    98% - {bin_accept_98}\n"
        "\n"
        "REJECTs\n"
        f"    50% - {reject_50}\n"
        f"    90% - {reject_90}\n"
        f"    98% - {reject_98}\n"
        "\n"
        "binNEW REJECTs\n"
        f"    50% - {bin_reject_50}\n"
        f"    90% - {bin_reject_90}\n"
        f"    98% - {bin_reject_98}\n"
        "\n"
    )


def eval_by_month(db, output):
    def days_at_index(array, index):
        if index < len(array):
            return array[index].days + array[index].seconds / 86400
        return float("inf")

    uploaded = defaultdict(int)
    done = defaultdict(int)
    times = defaultdict(list)
    logging.info("Processing queue events")
    for action, is_bin_new, entry_time, exit_time, changes_file in queue_events(db):
        year_mon = entry_time.strftime("%Y-%m")
        if action == "NEW":
            uploaded[year_mon] += 1
        if action == "ACCEPT" or action == "REJECT":
            times[year_mon].append(exit_time - entry_time)
            done[exit_time.strftime("%Y-%m")] += 1

    output.write("# month  p50     p90     p98     uploads done\n")

    for year_mon in sorted(uploaded.keys()):
        total = uploaded[year_mon]
        month_done = done[year_mon]
        month_times = sorted(times[year_mon])
        p50 = days_at_index(month_times, int(0.50 * total))
        p90 = days_at_index(month_times, int(0.90 * total))
        p98 = days_at_index(month_times, int(0.98 * total))
        output.write(
            f"{year_mon}  {p50:<8.1f}{p90:<8.1f}{p98:<8.1f}{total:<8}{month_done}\n"
        )


def main():
    log_dirs = [Path("./log"), Path("/srv/ftp-master.debian.org/log")]
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--loglevel",
        metavar="LEVEL",
        default="info",
        help="set logging level (debug, info, warning, error); default: info",
    )
    parser.add_argument(
        "--input",
        "-I",
        metavar="DIR",
        action="append",
        default=[],
        type=Path,
        help=f"read ftp-master log files from DIR",
    )
    parser.add_argument(
        "--dbfile",
        "-d",
        metavar="FILE",
        default="new_queue.db",
        help="sqlite3 database file",
    )
    parser.add_argument(
        "--output",
        "-o",
        metavar="FILE",
        default=sys.stdout,
        type=argparse.FileType("w", encoding="utf-8"),
        help="write output to FILE",
    )
    parser.add_argument(
        "--update",
        "-u",
        action="store_true",
        default=True,
        help="update database from FTP log files",
    )
    parser.add_argument(
        "--no-update",
        "-n",
        action="store_false",
        dest="update",
        help="do not update database from FTP log files",
    )
    parser.add_argument(
        "--show", "-s", default="month", help="show statistics (month, outcome)"
    )
    args = parser.parse_args()
    loglevel = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(loglevel, int):
        raise ValueError(f"invalid log level: {args.loglevel}")
    logging.basicConfig(format="%(levelname).1s: %(message)s", level=loglevel)
    if args.input:
        log_dirs = args.input
    db = sqlite3.connect(args.dbfile, detect_types=sqlite3.PARSE_DECLTYPES)
    if args.update:
        update_database(db, log_dirs)
    if args.show == "month":
        eval_by_month(db, args.output)
    elif args.show == "outcome":
        eval_by_outcome(db, args.output)


if __name__ == "__main__":
    main()
