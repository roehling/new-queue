#
# Copyright 2021 Timo Röhling <roehling@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
REMOTE = 1
SVGS = new_time.svg new_time-dark.svg
MIRROR = coccia.debian.org

all: $(addprefix website/,$(SVGS))

website/%.svg: %.gnuplot new_queue.txt
	gnuplot -e "set output '$@'" $<
	-svgo $@

website/%-dark.svg: website/%.svg
	sed -e 's/#000/#ddd/g' $< > $@

website/index.html: website.html new_queue.txt
	sed -e 's/@LASTUPDATED@/$(shell date -R -rnew_queue.txt)/g' website.html > $@

upload: upload-stamp

upload-stamp: website/index.html website/new_queue.css $(addprefix website/,$(SVGS))
	rsync -rv --delete website/ people.debian.org:public_html/new_queue/
	touch $@

remote-script: remote-script-stamp

remote-script-stamp: new_queue.py
	scp $< $(MIRROR):
	touch $@

REMOTE_LOG_DIR = /srv/ftp-master.debian.org/log
fetch-log:
	mkdir -p log
	rsync -tv $(MIRROR):$(REMOTE_LOG_DIR)/\*.xz log/

ifeq ($(REMOTE),0)
LOG_DIR = ./log
new_queue.txt: $(wildcard $(LOG_DIR)/*)
	./new_queue.py -o $@
else
new_queue.txt: remote-script-stamp new-queue-stamp
	ssh $(MIRROR) ./new_queue.py > $@
new-queue-stamp: FORCE
	touch -d "12 hours ago" $@
FORCE:
.PHONY: FORCE
endif

.PHONY: upload
