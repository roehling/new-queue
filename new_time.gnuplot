#
# Copyright 2021 Timo Röhling <roehling@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
set xdata time
set xrange [1346450400:*]
set timefmt "%Y-%m"
set xtics format "%b %Y" rotate
set mxtics time 3 months
set ylabel "Time spent in NEW"
set samples 1000
set logscale y 2
set ytics ("1 day" 1, "2 days" 2, "3 days" 3, "4 days" 4, "5 days" 5, "" 6, "1 week" 7, "2 weeks" 14, "3 weeks" 21, "1 month" 30, "2 months" 60, "3 months" 90, "" 120, "" 150, "6 months" 180, "1 year" 365, "2 years" 730)
set key below
set grid xtics ytics
set style line 1 lt 1 lc "blue" lw 2
set style line 2 lt 2 lc "orange" lw 2
set style line 3 lt 3 lc "red" lw 2
set style data lines

# Wheezy freeze
set obj 1 rectangle behind from first "2012-08", graph 0 to first "2013-05", graph 1 back
set obj 1 fillstyle solid 0.1 noborder fillcolor "red"
# Jessie freeze
set obj 2 rectangle behind from first "2014-11", graph 0 to first "2015-05", graph 1 back
set obj 2 fillstyle solid 0.1 noborder fillcolor "red"
# Stretch freeze
set obj 3 rectangle behind from first "2017-01", graph 0 to first "2017-07", graph 1 back
set obj 3 fillstyle solid 0.1 noborder fillcolor "red"
# Buster freeze
set obj 4 rectangle behind from first "2019-02", graph 0 to first "2019-07", graph 1 back
set obj 4 fillstyle solid 0.1 noborder fillcolor "red"
# Bullseye freeze
set obj 5 rectangle behind from first "2021-02", graph 0 to first "2021-08", graph 1 back
set obj 5 fillstyle solid 0.1 noborder fillcolor "red"
# Bookworm freeze
set obj 6 rectangle behind from first "2023-02", graph 0 to first "2023-06", graph 1 back
set obj 6 fillstyle solid 0.1 noborder fillcolor "red"

set terminal svg size 1100,600 font "sans-serif,16" name "new_time"

plot "new_queue.txt" u 1:2 sm mcsplines t "50-percentile" ls 1, \
     "" u 1:3 sm mcsplines t "90-percentile" ls 2, \
     "" u 1:4 sm mcsplines t "98-percentile" ls 3
